Frogger Game
===============================

A classic Arcade Game clone in which the player has to reach to the Top of the game by avoiding contacts with the enemies.

How to play the game
=========
Use the keyboard buttons to move the player (up,down,left,right) and can select the different players during the game journey.During the journey collect the objects to gain some score to achieve maximum points. 

Want to play the game click here [arcadeGame](https://biradarsidduram.gitlab.io/fendArcadeGameClone/)

<!-- [guide](https://docs.google.com/document/d/1v01aScPjSWCCWQLIpFqvg3-vXLH2e8_SZQKC8jNO0Dc/pub?embedded=true). -->
