// Enemies our player must avoid
var Enemy = function(x,y,speed) {
    // Variables applied to each of our instances go here,
    // we've provided one for you to get started

    // The image/sprite for our enemies, this uses
    // a helper we've provided to easily load images
    this.name = "bettle";
    this.sprite = 'images/enemy-bug.png';
    this.x = x;
    this.y = y;
    this.speed = speed;
 };
// Update the enemy's position, required method for game
// Parameter: dt, a time delta between ticks
Enemy.prototype.update = function(dt) {
    // You should multiply any movement by the dt parameter
    // which will ensure the game runs at the same speed for
    // all computers.
    if(this.x>=500){
        this.x = -20;
        this.speed = Math.floor(Math.random()*500)+100;
     }
    this.x+=this.speed*dt;
 };
Enemy.prototype.render = function() {
     ctx.drawImage(Resources.get(this.sprite), this.x, this.y);
};

// Now write your own player class
// This class requires an update(), render() and
// a handleInput() method.

var Player = function(){
    //default location and character..
    this.sprite = 'images/char-boy.png';
    this.x = 200;
    this.y = 380;
}

Player.prototype.update = function(){
    // check player collision with game objects..
    collisionItems.forEach( function(element, index) {
        collision(element);
    });
    //if the player reaches top..
    if(this.y<0){
        window.setTimeout(function(){
            alert("Hurray!! you won the game");
            reset();
        },15);
    }
}

Player.prototype.render = function(){
    Enemy.prototype.render.call(this);
}

Player.prototype.handleInput = function(keypress){
    //boundary conditions for players..
    if(keypress === "left" && this.x >0)
        this.x-=100;
    if(keypress ==="right" && this.x < 400)
         this.x+=100;
    if(keypress ==="down" && this.y < 380)
         this.y+=80;
    if(keypress ==="up" && this.y >0)
        this.y-=80;
} 
// Now instantiate your objects.
// Place all enemy objects in an array called allEnemies
// Place the player object in a variable called player

let yPos = [60,140,220]; // vertical pos differ by 80
let xPos = [0,100,200,300,400]; // horizontal pos differ by 100
let player = new Player();
let allEnemies = [];
for(var i =0;i<3;i++){
    allEnemies[i] = new Enemy(-10,yPos[i],Math.floor(Math.random()*500)+100);
}

//declared the stone,heart,star object and  render on to the screen..

let Elem = function(x,y,sprite,name){
    this.name = name;    
    this.x = x;
    this.y = y;
    this.sprite = sprite;
}
Elem.prototype.render = function(){
    Enemy.prototype.render.call(this);
}

let rock = new Elem(xPos[Math.floor(Math.random()*5)],yPos[0],'images/Rock.png','rock');
let ind = Math.floor(Math.random()*2)+1;
let heart = new Elem(xPos[Math.floor(Math.random()*5)],yPos[ind],'images/Heart.png','heart');
Object.assign(heart,{value:500});
ind = ind==2 ? 1 : 2;
let star = new Elem(xPos[Math.floor(Math.random()*5)],yPos[ind],'images/Star.png','star');
Object.assign(star,{value:1500});
let collisionItems = allEnemies.map( data => data);
collisionItems.push(rock);
collisionItems.push(heart);
collisionItems.push(star);
let players = [];
players.push('images/char-boy.png');
players.push('images/char-horn-girl.png');
players.push('images/char-cat-girl.png');
players.push('images/char-pink-girl.png');
players.push('images/char-princess-girl.png');

// This listens for key presses and sends the keys to your
// Player.handleInput() method. You don't need to modify this.
document.addEventListener('keyup', function(e) {
    var allowedKeys = {
        37: 'left',
        38: 'up',
        39: 'right',
        40: 'down'
    };
    player.handleInput(allowedKeys[e.keyCode]);
});
function scoreUpdate(val){
    let score = document.querySelector('#score span');
    if(val===0)
        score.textContent = 0;  
    score.textContent = parseInt(score.textContent)+val;
}
function collision(item){
    if (item.x===player.x && item.y===player.y) {
        if (item.name==='star' || item.name==='heart') {
              //hide the elements..
            item.x=item.y=-100;
            scoreUpdate(item.value);
        }
        else if(item.name==='rock'){
            //reset the position
            resetPlayerPos();
            alert("Rock holds you from your destination");
        }
    }
    else if(item.name==='bettle'){
            // reset the game    
        if( (Math.abs(item.x-player.x) <80 || item.x===player.x) && Math.abs(item.y-player.y)<10 && (rock.x!==player.x || rock.y!==player.y)){
            window.setTimeout(function(){
                alert("bug killed me");
                reset();
            },10);
        }
    }
}
function resetPlayerPos(){
    player.x=200;
    player.y =380;
}
function reset(){
    resetPlayerPos();
    ind = Math.floor(Math.random()*2)+1;
    rock.x = xPos[Math.floor(Math.random()*5)];
    heart.x = xPos[Math.floor(Math.random()*5)];
    heart.y = yPos[ind];
    ind = ind==2 ? 1 : 2;
    star.x = xPos[Math.floor(Math.random()*5)];
    star.y = yPos[ind];
    scoreUpdate(0);
}